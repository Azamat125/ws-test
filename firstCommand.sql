CREATE DATABASE test;

show databases;
use test;

CREATE TABLE persons (
    personID int NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    password varchar(100) NOT NULL,
    PRIMARY KEY (personID),
    UNIQUE (name)
);
CREATE TABLE sessions (
    sessionID int NOT NULL AUTO_INCREMENT,
    token varchar(500),
    personID int,
    PRIMARY KEY (sessionID),
    FOREIGN KEY (personID) REFERENCES Persons(personID),
    UNIQUE (token)
);
CREATE TABLE messages (
    messagesID int NOT NULL AUTO_INCREMENT,
    message varchar(500),
    personID int,
    PRIMARY KEY (messagesID),
    FOREIGN KEY (personID) REFERENCES persons(personID)
);