// native server with express
const express = require('express');
const app = express();
const port = 3001;
const connection = require('./src/config/db.config');
const mainROuter = require('./src/routes/main.router');
const appMiddleware = require('./src/middlewares/app.middleware');
appMiddleware.forEach((middleware) => app.use(middleware));

app.use(mainROuter);
const start = () => {
  connection.connect((err) => {
    if (err) {
      return console.log(`Error with connect to Database, ${err.message}`);
    } else {
      console.log('Connected Database');
    }
  });
  app.listen(port, () => {
    console.log(`server started on port: ${port}`);
  });
};

start();

// server with websocket
const http = require('http');
const WebSocket = require('ws');
const connectionMYSQL = require('./src/config/db.config');
const portWS = 3002;
const helpers = require('./src/helpers/helpers');
const server = http.createServer(express);
const wss = new WebSocket.Server({ server });

wss.on('connection', (ws) => {
  ws.on('message', (data) => {
    const obj = JSON.parse(data);
    const token = obj.message.split(' ');
    const isHistory = obj.message.includes('history');

    if (isHistory) {
      connectionMYSQL
        .promise()
        .query(`SELECT * FROM persons WHERE name = '${obj.name}' `)
        .then(([result]) => {
          connectionMYSQL.query(
            `INSERT INTO messages (message,personID) VALUES ('${obj.message}','${result[0].personID}')`
          );
          //   нахожу все сообщения пользователя
          connectionMYSQL
            .promise()
            .query(
              `SELECT * FROM messages WHERE personID = '${result[0].personID}' LIMIT 10 `
            )
            .then(([result]) => {
              console.log(result);
              result.forEach((element) => {
                ws.send(element.message);
              });
            });
        })
        .catch((e) => {
          console.log('error');
        });
    }

    if (helpers.checkToken(token[1])) {
      // делаю сначала выборку по клиенту, нахожу по входящему имени клиента его ID в БД
      // далее этот ID  передаю в запрос на создание записи в таблицу messages
      connectionMYSQL
        .promise()
        .query(`SELECT * FROM persons WHERE name = '${obj.name}' `)
        .then(([result]) => {
          connectionMYSQL.query(
            `INSERT INTO messages (message,personID) VALUES ('${obj.message}','${result[0].personID}')`
          );
          //   нахожу все сообщения пользователя
          connectionMYSQL
            .promise()
            .query(
              `SELECT * FROM messages WHERE personID = '${result[0].personID}' `
            )
            .then(([result]) => {
              // отправляю всем подключенным клиентам все сообщения данного пользователя
              wss.clients.forEach((client) => {
                client.send(JSON.stringify(result));
              });
            });
        })
        .catch((e) => {
          console.log('error');
        });
    }
  });
});

server.listen(portWS, () => {
  console.log('websocket server started on port, ', portWS);
});
