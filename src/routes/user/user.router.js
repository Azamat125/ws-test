const {Router} = require("express")
const {check } = require("express-validator")
const UsersControllers = require("./controllers");


const userRouter = Router();


userRouter.post("/",
    [
        check("name","Name length should be 5 to 10 characters").isLength({min:5,max:10}),
        check("password","Password length should be 8 to 10 characters").isLength({ min: 8, max: 10 })
    ],UsersControllers.createUser);

userRouter.post("/session",UsersControllers.loginUser);
module.exports = userRouter;