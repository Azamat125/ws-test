const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const salt = bcrypt.genSaltSync(10);
const connection = require('../../config/db.config');
const { secretJWT } = require('../../config/general.config');

const UsersControllers = {
  async createUser(req, res) {
    try {
      const { name, password } = req.body;
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.send({ message: errors });
      }
      const hash = await bcrypt.hash(password, salt);

      connection.query(
        `INSERT INTO persons (name,password) VALUES ('${name}','${hash}')`,
        function (err, results) {
          if (err) {
            return res.send({ message: err });
          }
          return res.send({ message: 'success' });
        }
      );
    } catch (e) {
      return res.send({ message: e.message });
    }
  },

  async loginUser(req, res) {
    try {
      const { name, password } = req.body;

      connection
        .promise()
        .query(`SELECT * FROM persons persons WHERE name = '${name}' `)
        .then(([result]) => {
          const isValidPassword = bcrypt.compareSync(
            password,
            result[0].password
          );
          if (isValidPassword) {
            const token = jwt.sign(
              {
                username: name,
              },
              secretJWT,
              { expiresIn: '2h' }
            );
            connection.query(
              `INSERT INTO sessions (token,personID) VALUES ('${token}','${result[0].personID}')`,
              function (err, results) {
                if (err) {
                  return res.send({ message: err });
                }
                return res.send({ token: token });
              }
            );
          } else {
            return res.send({ message: 'wrong password or name' });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      return res.send({ message: e.message });
    }
  },
};

module.exports = UsersControllers;
