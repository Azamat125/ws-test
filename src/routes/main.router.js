const {Router} = require("express");
const mainRouter = Router();

const userRouter = require("./user/user.router");

mainRouter.use("/users",userRouter);

module.exports = mainRouter;