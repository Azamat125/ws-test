const jwt = require('jsonwebtoken');
const config = require('../config/general.config');
module.exports = {
  checkToken: (token) => {
    return jwt.verify(token, config.secretJWT, (err, decoded) => {
      if (err) {
        return null;
      }
      return decoded.username;
    });
  },
};
